﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roulette : MonoBehaviour
{
    [SerializeField] private Rotator rotator = null;
    [SerializeField] private Transform roulette = null;
    [Header("Degrees Per Second")]
    [SerializeField] private float maxRotationSpeed = 1500f;
    [SerializeField] private float animationToMaxSpeedTime = 5f;
    [SerializeField] private float animationMaxSpeedTime = 3f;
    [SerializeField] private float animationToEndTime = 7f;
    [SerializeField] private float speedToStop = 1f;

    private Coroutine animation = null;

    private void Awake()
    {
        Subscribe();
    }

    private void OnDestroy()
    {
        Unsubscribe();
        StopAllCoroutines();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void Subscribe()
    {
        if (rotator != null)
        {
            rotator.OnTouchRotationEnd += OnTouchRotationEnd;
        }
    }

    private void Unsubscribe()
    {
        if (rotator != null)
        {
            rotator.OnTouchRotationEnd -= OnTouchRotationEnd;
        }
    }

    private void OnTouchRotationEnd(RotationDirection dir, float speed)
    {
        rotator.SetTouchRotationAbility(false);

        var direction = GetRotationDirection(dir);
        if (animation != null)
        {
            StopCoroutine(animation);
        }
        animation = StartCoroutine(MakeAnimation(GetRotationDirection(direction), speed));
    }

    private IEnumerator MakeAnimation(RotationDirection dir, float startSpeed)
    {
        startSpeed = dir == RotationDirection.CW ? -startSpeed : startSpeed;
        var endSpeed = dir == RotationDirection.CW ? -maxRotationSpeed : maxRotationSpeed;

        var currentSpeed = startSpeed;

        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime / animationToMaxSpeedTime;
            currentSpeed = EasingFunction.EaseInOutQuart(startSpeed, endSpeed, t);
            roulette.Rotate(Vector3.forward, currentSpeed * Time.deltaTime);
            yield return null;
        }

        t = 0;
        while (t < 1f)
        {
            t += Time.deltaTime / animationMaxSpeedTime;
            roulette.Rotate(Vector3.forward, currentSpeed * Time.deltaTime);
            yield return null;
        }

        startSpeed = currentSpeed;
        endSpeed = 0f;
        t = 0;
        while (t < 1f)
        {
            t += Time.deltaTime / animationToEndTime;
            currentSpeed = EasingFunction.EaseOutQuint(startSpeed, endSpeed, t);
            roulette.Rotate(Vector3.forward, currentSpeed * Time.deltaTime);
            yield return null;
            if (Mathf.Abs(currentSpeed) < speedToStop)
            {
                break;
            }
        }

        animation = null;
        rotator.SetTouchRotationAbility(true);
        Debug.Log("ended");
    }

    private RotationDirection GetRotationDirection(RotationDirection currentDirection)
    {
        return currentDirection == RotationDirection.None ? GetRandomDirection() : currentDirection;
    }

    private RotationDirection GetRandomDirection()
    {
        return Random.Range(0, 2) == 0 ? RotationDirection.CW : RotationDirection.CW;
    }
}