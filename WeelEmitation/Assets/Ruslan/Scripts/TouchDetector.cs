﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchDetector : MonoBehaviour, IBeginDragHandler, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    public event Action<Vector2> OnUp;
    public event Action<Vector2> OnDown;
    public event Action<Vector2> OnDrag;
    public event Action<Vector2> OnBeginDrag;

    [SerializeField] private Transform touchableObjectTransform;

    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
    {
        OnBeginDrag?.Invoke(VectorUtility.GetWorldPositionOnPlane(Input.mousePosition, 100) - touchableObjectTransform.position);
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        OnDown?.Invoke(VectorUtility.GetWorldPositionOnPlane(Input.mousePosition, 100) - touchableObjectTransform.position);
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        OnDrag?.Invoke(VectorUtility.GetWorldPositionOnPlane(Input.mousePosition, 100) - touchableObjectTransform.position);
    }

    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        OnUp?.Invoke(VectorUtility.GetWorldPositionOnPlane(Input.mousePosition, 100) - touchableObjectTransform.position);
    }
}