﻿using UnityEngine;

public static class VectorUtility
{
    public static Vector2 CastToVector2(Vector3 v3, bool isZtoY)
    {
        return new Vector2(v3.x, isZtoY ? v3.z : v3.y);
    }

    public static Vector2 GetWorldPosByCamera(Vector2 v2)
    {
        return CastToVector2(Camera.main.ScreenToWorldPoint(v2), false);
    }

    public static Vector2 GetLocalPosByCamera(Vector2 v2, Vector3 objectPos)
    {
        return GetWorldPosByCamera(v2) - CastToVector2(objectPos, false);
    }

    public static Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPosition);
        Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
        float distance;
        xy.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
}