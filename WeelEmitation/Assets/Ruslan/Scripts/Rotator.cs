﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rotator : MonoBehaviour
{
    public event Action<RotationDirection, float> OnTouchRotationEnd;

    [SerializeField] private TouchDetector touchDetector = null;
    [SerializeField] private Transform rotatableTransform = null;
    [SerializeField] private float followSpeed = 4f;
    [SerializeField] private bool canRotate = true;
    [SerializeField] private float clickDistance = 8f;

    private Vector2 startTouchPos;
    private float startTouchAngle;
    private Vector2 currentTouchPos;
    private Quaternion targetRotation;
    private Quaternion currentRotation;

    private float currentTouchAngleDistinction;

    private bool isRotationStarted = false;

    private bool IsOnClick { get { return Vector2.Distance(startTouchPos, currentTouchPos) <= clickDistance; } }

    private void OnEnable()
    {
        Subscribe();
        isRotationStarted = false;
    }

    private void OnDisable()
    {
        Unsubscribe();
        isRotationStarted = false;
    }

    private void Subscribe()
    {
        if (touchDetector != null)
        {
            touchDetector.OnDrag += OnTouchDrag;
            touchDetector.OnBeginDrag += OnTouchBeginDrag;
            touchDetector.OnDown += OnTouchDown;
            touchDetector.OnUp += OnTouchUp;
        }
    }

    private void Unsubscribe()
    {
        if (touchDetector != null)
        {
            touchDetector.OnDrag -= OnTouchDrag;
            touchDetector.OnBeginDrag -= OnTouchBeginDrag;
            touchDetector.OnDown -= OnTouchDown;
            touchDetector.OnUp -= OnTouchUp;
        }
    }

    public void SetTouchRotationAbility(bool state)
    {
        canRotate = state;
        SetDefaultState();
    }

    private void Update()
    {
        currentRotation = rotatableTransform.localRotation;
        if (canRotate)
        {
            FollowTouch();
        }
    }

    private void SetDefaultState()
    {
        startTouchPos = default(Vector2);
        startTouchAngle = default(float);
        currentTouchPos = default(Vector2);
        targetRotation = default(Quaternion);
        currentRotation = default(Quaternion);
        currentTouchAngleDistinction = default(float);
        isRotationStarted = false;
    }

    private void OnTouchDown(Vector2 v)
    {
        startTouchPos = v;
        startTouchAngle = currentRotation.eulerAngles.z;
    }

    private void OnTouchBeginDrag(Vector2 v)
    {
        SetCurrentTouchPosAndTargetAngle(v);
        isRotationStarted = true;
    }

    private void OnTouchDrag(Vector2 v)
    {
        SetCurrentTouchPosAndTargetAngle(v);
    }

    private void OnTouchUp(Vector2 v)
    {
        if (!canRotate || IsOnClick) return;
        OnTouchRotationEnd?.Invoke(GetCurrentRotationDirection(), GetCurrentRotationSpeed());
    }

    private void FollowTouch()
    {
        rotatableTransform.localRotation = Quaternion.Slerp(currentRotation, targetRotation, followSpeed * Time.deltaTime);
    }

    private Quaternion GetTargetAngle()
    {
        var start = GetAngleBy(startTouchPos);
        var current = GetAngleBy(currentTouchPos);
        var destination = current - start;
        var end = startTouchAngle + destination;

        currentTouchAngleDistinction = destination;

        return Quaternion.Euler(rotatableTransform.localEulerAngles.x, rotatableTransform.localEulerAngles.y, end);
    }

    private void SetCurrentTouchPosAndTargetAngle(Vector2 touchPos)
    {
        currentTouchPos = touchPos; 
        targetRotation = GetTargetAngle();
    }

    private float GetAngleBy(Vector2 vector)
    {
        vector.Normalize();
        return Mathf.Atan2(vector.y, vector.x) * Mathf.Rad2Deg;
    }

    private RotationDirection GetCurrentRotationDirection()
    {
        return isRotationStarted ? (startTouchPos.y < currentTouchPos.y ? RotationDirection.CW : RotationDirection.CCW) : RotationDirection.None;
    }

    private float GetCurrentRotationSpeed()
    {
        return isRotationStarted ? Quaternion.Angle(currentRotation, targetRotation) / Time.deltaTime / followSpeed : 0f;
    }
}

public enum RotationDirection
{
    CCW, //counter-clockwise
    CW, //clockwise 
    None,
}