﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Collider2D))]

public class Wheel : MonoBehaviour
{
    [Header("Touch properties")]

    [Range(1, 10)]
    [SerializeField]
    [Tooltip("The sensivity of spining.")]
    private float rotationSpeed = 1f;

    [SerializeField]
    [Tooltip("This vector should be setted acoarding to initial position of wheel in the world orientation. " +
             "It should always be directed perpendicular to wheel and go from back of the wheel.")]
    private Vector3 spinAround = Vector3.forward;

    [SerializeField]
    [Tooltip("Camera that look at wheel. It should be orthographic")]
    private new Camera camera = default(Camera);


    [Header("General spining properties")]
    
    public List<string> Prizes;

    [SerializeField]
    private List<AnimationCurve> SpiningCurves;

    [SerializeField]
    private SpinForce spinForce = SpinForce.Middle;

    public Action OnSpiningEnd;
    public Action OnPiningBegin;

    
    public int SelectedItemNumber
    {
        get { return selectedItemNumber; }
        set
        {
            selectedItemNumber = value;
            isPickedOutside = true;
            Debug.Log("Wining prise picked outside");
        }
    }

    private Vector3 currentRotation;
    private const float CIRCLE_LENGTH = 360f;
    private bool isSpinning;
    private float wheelZoneSize;
    private int selectedItemNumber;
    private bool isStartAfterDraging = false;
    private float currentPosition;
    private Vector2 mousePositionVector;
    private Vector2 vectorToMouseFromWheel;
    private Vector2 vectorToMouse;
    private bool isRotatingClockwise;
    private bool isPickedOutside;
    
    private void Start()
    {
        GetComponent<Collider2D>().isTrigger = true;
        if (camera == null) camera = Camera.main;
        if (camera.orthographic == false)
        {
            Debug.LogWarning("Camera should be orthographic!");
        }

        isSpinning = false;
        wheelZoneSize = CIRCLE_LENGTH / Prizes.Count;
    }
    
    
    private void Update()
    {
        mousePositionVector = camera.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
        vectorToMouse = Vector2.Lerp(vectorToMouse, vectorToMouseFromWheel, 1 / rotationSpeed);

        Debug.DrawLine(mousePositionVector,vectorToMouse);
        if ((Input.GetKeyDown(KeyCode.Space) || isStartAfterDraging) && !isSpinning)
        {
            OnPiningBegin.Invoke();
            if (!isPickedOutside)
            {
                selectedItemNumber = Random.Range(0, Prizes.Count);
            }

            int numberOfFullCicle = 0;
            float timeToSpin = 0f;

            switch (spinForce)
            {
                case SpinForce.Weak:
                    numberOfFullCicle = (int)spinForce;
                    timeToSpin = 2.5f;
                    break;
                case SpinForce.Middle:
                    numberOfFullCicle = (int)spinForce;
                    timeToSpin = 4f;
                    break;
                case SpinForce.Strong:
                    timeToSpin = 5f;
                    numberOfFullCicle = (int)spinForce;
                    break;
            }

            float fewFullCicles = CIRCLE_LENGTH * numberOfFullCicle;
            float rangeToSelectedItemFromStart = (selectedItemNumber * wheelZoneSize);
            float angleToStart = (CIRCLE_LENGTH - currentPosition);

            float distanationAngle = fewFullCicles + rangeToSelectedItemFromStart + angleToStart;
            StartCoroutine(SpinTheWheel(timeToSpin, distanationAngle));
        }
    }


    private void OnMouseDrag()
    {
        vectorToMouseFromWheel = mousePositionVector - ToVector2(transform.position);
        var angleToRotate = Vector2.SignedAngle(vectorToMouseFromWheel, vectorToMouse);

        isRotatingClockwise = angleToRotate < 0;

        transform.rotation *= Quaternion.AngleAxis(-angleToRotate, spinAround);
    }

    void OnMouseUp()
    {
        isStartAfterDraging = true;
    }

    private IEnumerator SpinTheWheel(float timeToSpin, float distanationAngle)
    {
        isSpinning = true;
        float timer = 0.0f;
        int spiningCurves = Random.Range(0, SpiningCurves.Count);
        int spiningDirection = isRotatingClockwise ? 1 : -1;
        Debug.Log("Animation Curve No. : " + spiningCurves);

        while (timer < timeToSpin)
        {
            float angle = currentPosition + distanationAngle * spiningDirection * SpiningCurves[spiningCurves].Evaluate(timer / timeToSpin);

            transform.eulerAngles = new Vector3(0.0f, 0.0f, angle);
            timer += Time.deltaTime;
            yield return 0;
        }

        currentPosition = transform.localEulerAngles.z;
        isSpinning = false;
        
        Debug.Log("Prizes: " + Prizes[selectedItemNumber]);
        isStartAfterDraging = false;
        OnSpiningEnd.Invoke();
    }

    Vector2 ToVector2(Vector3 vector3)
    {
        return new Vector2(vector3.x, vector3.y);
    }

    enum SpinForce
    {
        Weak = 1,
        Middle = 4,
        Strong = 11
    }
}

