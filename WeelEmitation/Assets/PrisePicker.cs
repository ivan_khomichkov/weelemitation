﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrisePicker : MonoBehaviour
{
    //Example of seting the prise outsiode of wheel script. 
    //Works only for the first init
    
    public int prise = 5;
    private Slider slider;
    [SerializeField] private Text result;
    [SerializeField] private Text settingText;
    void Start()
    {
        slider= GetComponent<Slider>();
        result.text = String.Empty;
        var wheel = FindObjectOfType<Wheel>();
        wheel.SelectedItemNumber = prise;
        wheel.OnSpiningEnd += () =>
        {
            result.text = wheel.SelectedItemNumber.ToString();
            result.text += " " + wheel.Prizes[wheel.SelectedItemNumber]; 
        };

        wheel.OnPiningBegin += () =>
        {
            wheel.SelectedItemNumber = prise;
        };
    }

    void Update()
    {
        prise = (int)slider.value;
        settingText.text = slider.value.ToString();
    }
}
