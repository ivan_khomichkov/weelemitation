﻿using System.Collections;
using System.Collections.Generic;
using DG.DemiLib;
using UnityEngine;
using UnityEngine.EventSystems;

public class TestTouchDetect : MonoBehaviour, IPointerDownHandler, IDragHandler
{
    [SerializeField] private RectTransform transformToFollo;

    [SerializeField] private float secondsForFullCircle = 6; 
    // Use this for initialization
    void Start()
    {
        Debug.Log($"<color=green>start</color>");
        initialRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        initialVector = Vector2.Lerp(initialVector, pointer, 0.1f);
        transform.LookAt(initialVector);
        Debug.DrawLine(initialVector, transformToFollo.position, Color.yellow, 5f);
        Debug.Log($"<color=red>{Vector2.SignedAngle(initialVector, pointer)}</color>");

        Vector3 newDir = Vector3.RotateTowards(transform.right, pointer, 0.1f, 0.0f);
        Debug.DrawRay(transform.position, newDir, Color.red);

        // Move our position a step closer to the target.
        transform.rotation = Quaternion.LookRotation(newDir);

    }

    private void Awake()
    {
        GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
    }

   
    private Quaternion initialRotation;
    private Vector2 initialVector;
    public void OnPointerDown(PointerEventData eventData)
    {
        var rectTransform = GetComponent<RectTransform>();

        initialVector = eventData.position;

        Debug.DrawLine(initialVector, rectTransform.position, Color.green, 10f);
    }

    private Vector3 pointer;
    public void OnDrag(PointerEventData eventData)
    {
        var rectTransform = GetComponent<RectTransform>();
        Vector2 rectTransform2 = new Vector2(rectTransform.position.x, rectTransform.position.y);
        Debug.DrawLine(eventData.position, rectTransform.position, Color.red, 5f);


        pointer = eventData.position;
        rectTransform.LookAt(transformToFollo, Vector3.back);
        Debug.Log($"<color=red>{Vector2.Angle(rectTransform2 - initialVector, rectTransform2 - eventData.position)}</color>");
        Debug.Log($"<color=green>{Quaternion.AngleAxis(Vector2.SignedAngle(rectTransform2 - initialVector, rectTransform2 - eventData.position), Vector3.forward)}</color>");
        //rectTransform.rotation = initialRotation * Quaternion.AngleAxis(Vector2.SignedAngle(rectTransform2 - initialVector, rectTransform2 - eventData.position), Vector3.forward);
        rectTransform.rotation = initialRotation * Quaternion.AngleAxis(360f / 60f, Vector3.forward);
    }
}
