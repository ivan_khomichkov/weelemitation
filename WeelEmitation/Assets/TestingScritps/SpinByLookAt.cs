﻿using UnityEngine;
using System.Collections;
 
public class SpinByLookAt : MonoBehaviour
{
    public Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Debug.Log("test");
        Debug.LogError("test");
        Debug.LogWarning("test");
        if (Input.GetMouseButton(0))
        {
            
            var distance = Vector3.Distance(Camera.main.transform.position, transform.position); 
           // Vector2 touchDeltaPosition = Input.mousePosition;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition).direction.normalized;
            transform.LookAt(ray * distance);
            //rb.AddTorque(Camera.main.transform.up * -touchDeltaPosition.x);
            //rb.AddTorque(Camera.main.transform.right * touchDeltaPosition.y);
        }
    }
}