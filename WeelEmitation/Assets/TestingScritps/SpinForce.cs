﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class SpinForce : MonoBehaviour
{
    [SerializeField] private float TorqueForce = 200f;
    [SerializeField] private float WheelMass = 10f;
    [SerializeField] private float AngularDrag = 0.2f;

   
    private Rigidbody2D ridgetBody;

	// Use this for initialization
	void Start ()
	{
	    ridgetBody = GetComponent<Rigidbody2D>();
	    ridgetBody.mass = WheelMass;
	    ridgetBody.angularDrag = AngularDrag;
	    ridgetBody.constraints = RigidbodyConstraints2D.FreezePosition;

	}

    //private void OnMouseDrag()
    //{
    //    ridgetBody.angularDrag = 0;
    //    ridgetBody.AddTorque(TorqueForce);
    //}

    //private void OnMouseUp()
    //{
    //    ridgetBody.angularDrag = AngularDrag;
    //}

    //private void OnDrawGizmos()
    //{
    //    Gizmos.color=Color.blue;
    //    Gizmos.DrawLine(Camera.main.transform.position, Camera.main.ScreenPointToRay(Input.mousePosition).origin);
    //}

    private void Update()
    {
        RaycastHit hit;
 
        if (Input.GetMouseButton(0))
        {
            if (Physics.Raycast (Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            {
                
                Debug.Log("Got raycast"+hit.collider.name);
                float x = -Input.GetAxis("Mouse X");
                float y = -Input.GetAxis("Mouse Y");
                float speed = 10;
                transform.rotation *= Quaternion.AngleAxis(x*speed, Vector3.up);
                transform.rotation *= Quaternion.AngleAxis(y*speed, Vector3.right);
            }
            else
            {
                Debug.Log("No raycast");
            }
        }
    }
}
